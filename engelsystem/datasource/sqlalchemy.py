from __future__ import absolute_import, unicode_literals

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


class _DatasourceSqlalchemy(object):
    def __init__(self, *args, **kw):
        self.engine = engine = create_engine(*args, **kw)
        self.Session = sessionmaker(bind=engine, expire_on_commit=False)


class DatasourceSqlalchemy(_DatasourceSqlalchemy):
    def __init__(self, *args, **kw):
        super(DatasourceSqlalchemy, self).__init__(*args, **kw)

        self.session = self.Session()


try:
    from flask import g as flaskg
except ImportError:
    pass
else:
    class DatasourceSqlalchemyFlask(_DatasourceSqlalchemy):
        def __init__(self, app, *args, **kw):
            super(DatasourceSqlalchemyFlask, self).__init__(*args, **kw)

            @app.before_request
            def before_request():
                flaskg.__sqlalchemy_session = self.Session()

            @app.teardown_request
            def teardown_request(exception):
                flaskg.__sqlalchemy_session.close()

        @property
        def session(self):
            return flaskg.__sqlalchemy_session

