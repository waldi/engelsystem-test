from __future__ import absolute_import, unicode_literals

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from .....datasource.sqlalchemy import DatasourceSqlalchemy
from ..room import *

datasource = DatasourceSqlalchemy('sqlite:///:memory:', echo=True)
Base.metadata.create_all(datasource.engine)
rooms = Rooms(datasource)


class TestRoom(object):
    def test_create(self):
        r = rooms.new('Test')
        r.id = '1'
        rooms.create(r)
        rooms._session.commit()
        assert r.id == '1'

    def test_read(self):
        r = rooms.read('1')
        rooms._session.commit()
        assert r.id == '1'
        assert r.name == 'Test'
        assert r.description is None

    def test_update(self):
        c = rooms.new_update()
        c['name'] = 'Test1'
        rooms.update('1', c)
        rooms._session.commit()

    def test_delete(self):
        rooms.delete('1')
        rooms._session.commit()
