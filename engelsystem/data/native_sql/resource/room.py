from __future__ import absolute_import, unicode_literals

from sqlalchemy import delete, Column, String, Unicode

from .. import Base, _DataContextManager
from ..types import UUID


class Room(Base):
    __slots__ = ()
    __tablename__ = 'rooms'

    id = Column(UUID, primary_key=True, nullable=False, default=UUID.default_uuid4)
    # TODO: tagged string
    name = Column(Unicode, nullable=False)
    # TODO: tagged string
    description = Column(Unicode)

    def __init__(self, name=None, description=None):
        self.name, self.description = name, description


class Rooms(_DataContextManager):
    """
    Data storage for Room objects.
    """
    __slots__ = ()

    new = Room
    new_update = dict

    def create(self, obj):
        return self._session.add(obj)

    def read(self, id, _cls=Room):
        return self._session.query(_cls).filter(_cls.id == id).one()

    def update(self, id, update, _cls=Room):
        return self._session.query(_cls).filter(_cls.id == id).update(update)

    def delete(self, id, _cls=Room):
        return self._session.query(_cls).filter(_cls.id == id).delete()
