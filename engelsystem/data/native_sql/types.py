from sqlalchemy import types as sqltypes
from sqlalchemy.dialects.postgresql.base import PGDialect, PGUuid
import uuid


class UUID(sqltypes.TypeDecorator):
    """
    Wrapper around PostgreSQL specific UUID data type.

    This wrapper uses sqlalchemy.types.String for other databases.
    """
    impl = sqltypes.TypeEngine

    @staticmethod
    def default_uuid4():
        return str(uuid.uuid4())

    def load_dialect_impl(self, dialect):
        if isinstance(dialect, PGDialect):
            return PGUuid()
        else:
            return sqltypes.String()

