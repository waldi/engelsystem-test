from __future__ import absolute_import, unicode_literals

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class _DataContextManager(object):
    """
    Context manager for all data classes.

    Handles transactions on exit.
    """
    __slots__ = '_datasource'

    def __init__(self, datasource):
        self._datasource = datasource

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        t = self._datasource.session.transaction
        return t.__exit__(type, value, traceback)

    @property
    def _session(self):
        return self._datasource.session


class _DataResource(_DataContextManager):
    def __init__(self, datasource):
        super(_DataResource, self).__init__(datasource)

        from .resource.room import Rooms
        self.room = Rooms(datasource)


class DataNativeSql(_DataContextManager):
    """
    Native SQLAlchemy-only data store.
    """
    def __init__(self, datasource):
        super(DataNativeSql, self).__init__(datasource)

        self.resource = _DataResource(datasource)

    def create_all(self):
        Base.metadata.create_all(self._datasource.engine)

