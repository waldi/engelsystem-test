#!/usr/bin/python
# -*- coding: utf-8 -*-
# Copyright: 2011 Bastian Blank <bastian@waldi.eu.org>
# License: GNU GPL v3 (or any later version), see COPYING for details.

import logging
import logging.config
import os.path

#logging.config.fileConfig(os.path.join(os.path.dirname(__file__), 'logging.conf'))

from engelsystem.app import create_app

app = create_app()
app.run(debug=True)
