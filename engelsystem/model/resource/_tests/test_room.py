from __future__ import absolute_import, unicode_literals

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from ....datasource.sqlalchemy import DatasourceSqlalchemy
from ....data.native_sql import DataNativeSql
from ..room import *

datasource = DatasourceSqlalchemy('sqlite:///:memory:', echo=True)
data = DataNativeSql(datasource)
data.create_all()
model = ModelRoom(data.resource.room)

class Test(object):
    def test_create(self):
        t = model.new('Test')
        assert t
        assert t.name == 'Test'
        model.create(t)
