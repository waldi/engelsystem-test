# TODO: Access control
class ModelRoom(object):
    __slots__ = '__data'

    def __init__(self, data):
        self.__data = data

    @property
    def new(self):
        return self.__data.new

    @property
    def new_update(self):
        return self.__data.new_update

    def create(self, room):
        with self.__data as data:
            return data.create(room)

    def read(self, id):
        with self.__data as data:
            return data.read(id)

    def update(self, id, update):
        with self.__data as data:
            return data.update(id, update)

    def delete(self, id):
        with self.__data as data:
            return data.delete(id)

