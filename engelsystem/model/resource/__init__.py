try:
    from flask import g
except ImportError: pass

from .room import ModelRoom


class ModelResource(object):
    __slots__ = '__data'

    def __init__(self, data):
        self.__data = data

    def room(self):
        return ModelRoom(self.__data.resource_room(g.db_session))
