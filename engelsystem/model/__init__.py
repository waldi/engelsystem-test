from __future__ import absolute_import, unicode_literals


class _ModelResource(object):
    def __init__(self, data):
        from .resource.room import ModelRoom
        self.room = ModelRoom(data.room)


class Model(object):
    def __init__(self, data):
        self.data = data

        self.resource = _ModelResource(data.resource)
