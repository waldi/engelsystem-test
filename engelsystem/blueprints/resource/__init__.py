from __future__ import absolute_import, unicode_literals

from flask import Blueprint

blueprint = Blueprint('resource', __name__, template_folder='templates')
blueprint_api = Blueprint('resource_api', __name__)

from . import controller

def setup(app):
    app.register_blueprint(blueprint, url_prefix='/resource')
    app.register_blueprint(blueprint_api, url_prefix='/api/v1/resource')

