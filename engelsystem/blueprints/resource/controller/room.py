from __future__ import absolute_import, unicode_literals

from flask import current_app, request, g, jsonify
from flask.views import MethodView

from .. import blueprint, blueprint_api


class RoomDefault(MethodView):
    def get(self):
        model = current_app.model.resource.room
        print model
        pass

    def post(self):
        pass


class RoomDefaultItem(MethodView):
    def get(self, id):
        pass

    def post(self, id):
        pass


class RoomApi(MethodView):
    def _jsonify(self, room):
        return jsonify(
                kind='test',
                id=room.id,
                name=room.name,
                description=room.description)

    def get(self, id=None):
        model = current_app.model.resource.room
        if id is None:
            pass
        else:
            return self._jsonify(model.read(id))

    def post(self):
        model = current_app.model.resource.room
        new = model.new()

        data = request.json
        if not data:
            return

        # XXX: Use real validation
        for key, value in data.iteritems():
            if isinstance(key, unicode):
                setattr(new, key, value)

        model.create(new)

        return self._jsonify(new)

    def put(self, id):
        model = current_app.model.resource.room
        update = model.new_update()

        data = request.json
        if not data:
            return

        # XXX: Use real validation
        for key, value in data.iteritems():
            if isinstance(key, unicode):
                update[key] = value

        model.update(id, update)

    def delete(self, id):
        model = current_app.model.resource.room
        model.delete(id)


blueprint.add_url_rule('/room/', view_func=RoomDefault.as_view(b'room_default'))
blueprint.add_url_rule('/room/<id>', view_func=RoomDefaultItem.as_view(b'room_default_item'))

api = RoomApi.as_view(b'room_api')
blueprint_api.add_url_rule('/room/', view_func=api, methods=('GET', 'POST'))
blueprint_api.add_url_rule('/room/<id>', view_func=api, methods=('GET', 'PUT', 'DELETE'))
