from __future__ import absolute_import, unicode_literals

from flask import Flask

def create_app():
    app = Flask('engelsystem')

    from .datasource.sqlalchemy import DatasourceSqlalchemyFlask
    datasource = DatasourceSqlalchemyFlask(app, 'sqlite:///test.db')
    from .data.native_sql import DataNativeSql
    data = DataNativeSql(datasource)
    from .model import Model
    app.model = Model(data)

    from .blueprints.resource import setup
    setup(app)

    return app

